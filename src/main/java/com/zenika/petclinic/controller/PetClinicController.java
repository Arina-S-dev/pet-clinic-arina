package com.zenika.petclinic.controller;
import com.zenika.petclinic.service.model.PetClinic;
import com.zenika.petclinic.controller.dto.PetClinicDto;
import com.zenika.petclinic.service.PetClinicService;
import com.zenika.petclinic.controller.dto.PetClinicMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/pet-clinics")
public class PetClinicController {

    private static final Logger logger = LoggerFactory.getLogger(PetClinicController.class);

    private PetClinicMapper petClinicMapper;

    private PetClinicService petClinicService;

    public PetClinicController(PetClinicMapper petClinicMapper, PetClinicService petClinicService) {
        this.petClinicMapper = petClinicMapper;
        this.petClinicService = petClinicService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<PetClinicDto> allClinics(){
        List<PetClinic> petClinicList = petClinicService.getClinicsList();
        List<PetClinicDto> petClinicDtoList = new ArrayList<>();
        for (PetClinic petClinic : petClinicList) {
            PetClinicDto petClinicDto = petClinicMapper.petClinicToPetClinicDto(petClinic);

            petClinicDtoList.add(petClinicDto);
        }
        return petClinicDtoList;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public PetClinicDto getPetClinicById(@PathVariable("id") int id) {
        PetClinic petClinic = petClinicService.getPetClinicById(id);
        return petClinicMapper.petClinicToPetClinicDto(petClinic);

    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public void addPetClinic(@RequestBody PetClinicDto petClinicDto){
       PetClinic petClinic = petClinicMapper.petClinicDtoToPetClinic(petClinicDto);
       petClinicService.addPetClinic(petClinic);
       logger.info("Ajout d'objet {}", petClinic);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public void updateClinic(@PathVariable("id") int id, @RequestBody PetClinicDto petClinicDto) {
        PetClinic petClinic = petClinicMapper.petClinicDtoToPetClinic(petClinicDto);
        petClinic.setId(id);
        petClinicService.updatePetClinic(petClinic);
        logger.info("Une demande de modification de clinique a été reçue. id:{}, clinique:{}", id, petClinic);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deletePetClinic(@PathVariable("id") int id) {
        logger.info("Une demande de suppression de clinique a été reçue :{}", id);
        petClinicService.removePetClinic(id);
    }
}
