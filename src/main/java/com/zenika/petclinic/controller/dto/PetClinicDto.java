package com.zenika.petclinic.controller.dto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PetClinicDto {

    private static final Logger logger = LoggerFactory.getLogger(PetClinicDto.class);
    private String name;
    private int id;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public PetClinicDto() {

         }
    public PetClinicDto(String name) {
        this.name = name;
    }
    public String getName() {
        return this.name;
    }

    public String setName(String name) {
        return this.name = name;
    }

    public String toString() {
        return  "" + getName() + " with id : " + getId();
    }
}
