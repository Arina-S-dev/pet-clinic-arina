package com.zenika.petclinic.controller.dto;

import com.zenika.petclinic.service.model.PetClinic;
import org.springframework.stereotype.Component;

@Component
public class PetClinicMapper {

    public PetClinic petClinicDtoToPetClinic(PetClinicDto petClinicDto){
        PetClinic petClinic = new PetClinic();
        petClinic.setName(petClinicDto.getName());
        petClinic.setId(petClinicDto.getId());
        return petClinic;
    }

    public PetClinicDto petClinicToPetClinicDto(PetClinic petClinic){
        PetClinicDto petClinicDto = new PetClinicDto();
        petClinicDto.setName(petClinic.getName());
        petClinicDto.setId(petClinic.getId());
        return petClinicDto;
    }
}
