package com.zenika.petclinic.service;

import com.zenika.petclinic.service.model.PetClinic;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class PetClinicService {

    public static List<PetClinic> petClinicList;

    public PetClinicService() {
        this.petClinicList = new ArrayList<>();
    }

    public static List<PetClinic> getClinicsList() {
        return petClinicList;
    }

    public static void addPetClinic(PetClinic petClinic) {
        petClinicList.add(petClinic);
        petClinic.setId(getNextId());
    }

    public PetClinic getPetClinicById(int id) {
        for (PetClinic petClinic : petClinicList) {
            if (petClinic.getId() == id) {
                return petClinic;
            }
        }
        return null;
    }

    public void removePetClinic(int id) {
        PetClinic petClinic = getPetClinicById(id);
        if (petClinic != null) {
            petClinicList.remove(petClinic);
        }
    }

    public void updatePetClinic(PetClinic petClinic) {
        PetClinic existingPetClinic = getPetClinicById(petClinic.getId());
        if (existingPetClinic == null) {
            petClinicList.add(petClinic);
        } else {
            existingPetClinic.setName(petClinic.getName());
        }
    }


    public static int getNextId() {
        final int nextId;
        if (petClinicList.isEmpty()) {
            nextId = 1;
        } else {
            nextId = Collections.max(petClinicList.stream().map(PetClinic::getId).toList()) + 1;
        }
        return nextId;
    }
}
